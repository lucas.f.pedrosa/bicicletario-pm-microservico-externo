package com.unirio.pm.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;

import com.unirio.pm.domain.Cartao;


public class JDBCMockCartaoTest {

	JDBCMockCartao mock = new JDBCMockCartao();
	
    @Test
    public void deleteCartao() {
    	assertEquals(true, mock.deleteData("1"));
    }

    @Test
    public void deleteFCartao() {
    	assertEquals(false, mock.deleteData("1asdad"));
    }
    
    @Test
    public void updateCartao() {
    	 String id = "232";
       String cvv = "359";
       String nome = "Lucas";
       String num = "23523553";
       String validade = "20/11/2020";
       Cartao cartao = Cartao.builder().id(id).cvv(cvv).nomeTitular(nome).numero(num).validade(validade).build();
       cartao.setCvv("999");
       mock.updateData(cartao);
    	assertEquals("999", cartao.getCvv());
    }
    
    @Test
    public void updateFCartao() {
    	 String id = "232";
         String cvv = "359";
         String nome = "Lucas";
         String num = "23523553";
         String validade = "20/11/2020";
         Cartao cartao = Cartao.builder().id(id).cvv(cvv).nomeTitular(nome).numero(num).validade(validade).build();
         mock.updateData(cartao);
      	assertNotEquals("999", cartao.getCvv());
    }
    
    @Test
    public void updateDataCartao() {
    	 String id = "232";
         String cvv = "359";
         String nome = "Lucas";
         String num = "23523553";
         String validade = "20/11/2020";
         Cartao cartao = Cartao.builder().id(id).cvv(cvv).nomeTitular(nome).numero(num).validade(validade).build();
         mock.updateData(cartao);
         cartao.setNomeTitular("Outro nome");
         mock.updateData(cartao);
      	assertEquals("232", cartao.getId());
    }
    
    @Test
    public void updateDataCartaoByCiclista() {
    	 String id = "232";
         String cvv = "359";
         String nome = "Lucas";
         String num = "23523553";
         String validade = "20/11/2020";
         String idCiclista = "1A";
         Cartao cartao = Cartao.builder().id(id).cvv(cvv).nomeTitular(nome).numero(num).idCiclista(idCiclista).validade(validade).build();
         mock.updateData(cartao);
         
         cartao.setNomeTitular("Outro nome");
         cartao.setIdCiclista("1a");
         mock.updateCartaoByIdCiclista(cartao);
      	assertEquals("232", cartao.getId());
    }
    

    

}

package com.unirio.pm.controllers;

import com.unirio.pm.domain.Mail;
import com.unirio.pm.domain.NovoEmail;
import com.unirio.pm.util.Error;
import com.unirio.pm.util.Validator;

import controllers.EmailController;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class EmailControllerTest {
    private EmailController emailController;

    @Before
    public void setup() {
        this.emailController = new EmailController();
    }

    @Test
    public void deveCriarEmail() {
        String email = "teste@teste.com";
        String mensagem = "teste";
        String id = "123";
        Mail em = Mail.builder().email(email).mensagem(mensagem).uuid(id).build();
        assertEquals(email, em.getEmail());
        assertEquals(mensagem, em.getMensagem());
        assertEquals(id, em.getUuid());
    }

    @Test
    public void deveCriarNovoEmail() {
        String email = "teste@teste.com";
        String mensagem = "teste";
        NovoEmail em = NovoEmail.builder().email(email).mensagem(mensagem).build();
        assertEquals(email, em.getEmail());
        assertEquals(mensagem, em.getMensagem());
    }

    @Test
    public void deveCriarError() {
        String id = "123";
        String cod = "422";
        String men = "mensagem";
        Error err = Error.builder().id(id).codigo(cod).mensagem(men).build();
        assertEquals(id, err.getId());
        assertEquals(cod, err.getCodigo());
        assertEquals(men, err.getMensagem());
    }

    @Test
    public void deveVerificarEmailExiste() {
        String email = "test@test.com";
        Mail existeEmail = Mail.builder().email(email).build();
        assertEquals(true, this.emailController.validaEmail(existeEmail.getEmail()));
    }

    @Test
    public void deveVerificarEmailvalido() {
        assertEquals(true, this.emailController.validaEmail("test@test.com"));
    }

    @Test
    public void deveNotificarEmail() {
        String email = "test@test.com";
        assertEquals(true, Validator.notificaEmail(email));
    }

    @Test
    public void deveDarErroPatternEmail() {
        String email = "test";
        assertEquals(false, Validator.notificaEmail(email));
    }
    
    @Test
    public void deleteEmail() {
        assertEquals(true, EmailController.mock.deleteData("1"));
    }
    
    @Test
    public void deleteFEmail() {
        assertEquals(false, EmailController.mock.deleteData("adads1"));
    }
    
    @Test
    public void updateEmail() {
    	int tam = EmailController.mock.banco.size();
    	Mail email = Mail.builder().email("emailTestando@tatat.com").build();
    	EmailController.mock.updateData(email);
        assertEquals(tam+1, EmailController.mock.banco.size());
    }
    
}

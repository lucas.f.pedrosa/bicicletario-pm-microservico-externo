package com.unirio.pm.domain;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class MailTest {

	public Mail email = Mail.builder().uuid("232").mensagem("359").email("Lucas@mail.com").build();
	
	 @Test
	 public void getUuidEmail() {
		 String id = email.getUuid();
	  assertEquals("232", id);
	 }
	 
	 @Test
	 public void getEmail() {
		 String campo = email.getEmail();
	  assertEquals("Lucas@mail.com", campo);
	 }
	 
	 @Test
	 public void getMSgemail() {
		 String campo = email.getMensagem();
	  assertEquals("359", campo);
	 }
	 
	 
	 @Test
	 public void setIdEmail() {
	   email.setUuid("100");
	   assertEquals("100", email.getUuid());
	 }
	 
	 @Test
	 public void setEmail() {
		  email.setEmail("Cem@mail.com");
	  assertEquals("Cem@mail.com", email.getEmail());
	 }
	 
	 @Test
	 public void setMsgEmail() {
	  email.setMensagem("Msg ");
	  assertEquals("Msg ", email.getMensagem());
	 }
	 
}

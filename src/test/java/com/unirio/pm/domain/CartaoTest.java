package com.unirio.pm.domain;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class CartaoTest {

	public Cartao cartao = Cartao.builder().id("232").cvv("359").nomeTitular("Lucas").numero("23523553").validade("20/11/2020").build();
	
	 @Test
	 public void getIdCartao() {
		 String id = cartao.getId();
	  assertEquals("232", id);
	 }
	 
	 @Test
	 public void getNomeCartao() {
		 String campo = cartao.getNomeTitular();
	  assertEquals("Lucas", campo);
	 }
	 
	 @Test
	 public void getCVVCartao() {
		 String campo = cartao.getCvv();
	  assertEquals("359", campo);
	 }
	 
	 @Test
	 public void getNumCartao() {
		 String campo = cartao.getNumero();
	  assertEquals("23523553", campo);
	 }
	 
	 @Test
	 public void getValidaCartao() {
		 String campo = cartao.getValidade();
	  assertEquals("20/11/2020", campo);
	 }

	 @Test
	 public void setIdCartao() {
	 cartao.setId("100");
	  assertEquals("100", cartao.getId());
	 }
	 
	 @Test
	 public void setNomeCartao() {
		  cartao.setNomeTitular("Cem");
	  assertEquals("Cem", cartao.getNomeTitular());
	 }
	 
	 @Test
	 public void setCVVCartao() {
	  cartao.setCvv("100");
	  assertEquals("100", cartao.getCvv());
	 }
	 
	 @Test
	 public void setNumCartao() {
	  cartao.setNumero("1234657981234567");
	  assertEquals("1234657981234567", cartao.getNumero());
	 }
	 
	 @Test
	 public void setValidaCartao() {
	  cartao.setValidade("2020-09-12");
	  assertEquals("2020-09-12", cartao.getValidade());
	 }
     
}

package com.unirio.pm.services;

import java.util.HashMap;
import java.util.Map;

import com.unirio.pm.domain.Cartao;

import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;

public class CardAPI {
	
	 private static final String API_KEY = "T16250_PUB_2wtn6w3unw764xnzg9b84egmwz3xi9hnt36q2r3tp6d9urkzck78ywvypxcz";
	 private static final String URL = "https://api.payway.com.au/rest/v1/";
	 private static final String CONTENT_TYPE = "application/x-www-form-urlencoded";
	 /**
	  * user: T16250 senha: T16250abc 
	 * 
	  */
	 
	 private CardAPI() {
		 throw new IllegalStateException("Utility class");
	}
	 
	 @SuppressWarnings({ "unchecked", "rawtypes" })
	public static boolean cadastrarCartao(Cartao cartao) {
		 /*
		  * https://api.payway.com.au/rest/v1/single-use-tokens?cvn=847&cardholderName=lucas&expiryDateYear=29&expiryDateMonth=02&cardNumber=4564710000000004
		  */
		 try {
			 
	        Map body = new HashMap<>();
			body.put("paymentMethod", "creditCard");
			body.put("cvn", cartao.getCvv());
	        body.put("cardholderName", cartao.getNomeTitular());
	        //padrao 2020-02-02
	        body.put("expiryDateYear", cartao.getValidade().substring(2, 4));
	        body.put("expiryDateMonth", cartao.getValidade().substring(5, 7));
	        body.put("cardNumber", cartao.getNumero());
			
			HttpResponse<JsonNode> response = Unirest.post(URL+"/single-use-tokens")
					 .header("Authorization", "Basic Auth")
					 .basicAuth(API_KEY, "")
//					 .accept(CONTENT_TYPE)
					 .header("content-Type", CONTENT_TYPE)
					 .body(body)
					 .asJson();
					  
			int status = response.getStatus();
			if(status == 200)
				return true;
			
		 }catch (Exception e) {
			 return false;
		 }
		 return false;
	 }
	 

}

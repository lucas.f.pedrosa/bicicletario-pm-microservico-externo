package com.unirio.pm.services;

import java.util.ArrayList;
import java.util.List;

import com.unirio.pm.domain.Cartao;
import com.unirio.pm.domain.Cartao.CartaoBuilder;

public class JDBCMockCartao {
	
	/** lista de cartaos */
	public final List<Cartao> banco = new ArrayList<>();

	    public JDBCMockCartao() {
	        for (int i = 0; i < 10; i++) {
	        	CartaoBuilder cartao = Cartao.builder().id(String.valueOf(i)).nomeTitular("nome"+String.valueOf(i));
	            banco.add(cartao.build());
	        }
	    }

	    public void updateData(Cartao card) {
	        if (this.getCartaoByIdCiclista(card.getId()) == null) {
	            banco.add(card);
	        }else{
	            for (Cartao cartao : banco) {
	                if (cartao.getId().equalsIgnoreCase(card.getId())) {
	                    banco.set(banco.indexOf(cartao), card);
	                    return;
	                }
	            }
	        }
	    }

	    public Boolean deleteData(String id) {
	        for (Cartao cartao : banco) {
	            if (cartao.getId() != null && cartao.getId().equals(id)) {
	                banco.remove(cartao);                
	                return true;
	            }
	        }
	        return false;
	    }

		public Cartao getCartaoByIdCiclista(String id) {
			for (Cartao cartao : banco) {
	            if (cartao.getIdCiclista() != null && id != null && cartao.getIdCiclista().equalsIgnoreCase(id)) {
	                return cartao;
	            }
	        }
	        return null;
		}

		public boolean updateCartaoByIdCiclista(Cartao cartaoUpdate) {
			for (Cartao cartao : banco) {
	            if (cartao.getIdCiclista() != null && cartao.getIdCiclista().equalsIgnoreCase(cartaoUpdate.getId())) {
	            	banco.set(banco.indexOf(cartao), cartaoUpdate);
	                return true;
	            }
	        }
			return false;
		}
}

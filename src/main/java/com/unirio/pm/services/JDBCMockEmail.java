package com.unirio.pm.services;

import java.util.ArrayList;
import java.util.List;

import com.unirio.pm.domain.Mail;
import com.unirio.pm.domain.Mail.MailBuilder;

public class JDBCMockEmail {
	
	/** lista de emails */
	public final List<Mail> banco = new ArrayList<>();
	
	/** aloca emails num repositorio **/
	    public JDBCMockEmail() {
	        for (int i = 0; i < 10; i++) {
	        	MailBuilder email = Mail.builder().uuid(String.valueOf(i)).email("email"+String.valueOf(i)+"@email.com");
	            banco.add(email.build());
	        }
	    }
	    /** atualiza os emails ou adiciona eles **/
	    public void updateData(Mail mailParam) {
	        if (!this.existeEmail(mailParam.getEmail())) {
	            banco.add(mailParam);
	        }else{
	            for (Mail email : banco) {
	                if (email.getEmail().equalsIgnoreCase(mailParam.getEmail())) {
	                    banco.set(banco.indexOf(email), mailParam);
	                }
	            }
	        }
	    }

	    public Boolean deleteData(String id) {
	        for (Mail email : banco) {
	            if (email.getUuid() != null && email.getUuid().equals(id)) {
	                banco.remove(email);                
	                return true;
	            }
	        }
	        return false;
	    }

		public boolean existeEmail(String emailParam) {
			for (Mail email : banco) {
	            if (email.getEmail() != null && emailParam != null && email.getEmail().equalsIgnoreCase(emailParam)) {
	                return true;
	            }
	        }
	        return false;
		}
}

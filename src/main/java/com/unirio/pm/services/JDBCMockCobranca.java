package com.unirio.pm.services;

import java.util.ArrayList;
import java.util.List;

import com.unirio.pm.domain.Cobranca;
import com.unirio.pm.domain.Cobranca.CobrancaBuilder;
import com.unirio.pm.domain.PagamentoStatus;

public class JDBCMockCobranca {
	
	/** lista de cobrancas */
	private List<Cobranca> banco = new ArrayList<>();

	    public JDBCMockCobranca() {
	        for (int i = 0; i < 10; i++) {
	        	CobrancaBuilder cobranca = Cobranca.builder().id(String.valueOf(i)).ciclista(String.valueOf(i)).
	        			valor(500+i).status(PagamentoStatus.PAGA).horaFinalizacao("12/02/2022 18:00").horaSolicitacao("12/02/2022 16:00");
	            banco.add(cobranca.build());
	        }
	    }

	    //adiciona ou atualiza
	    public boolean updateData(Cobranca card) {
	        if (this.getCobrancaById(card.getId()) == null && this.getCobrancaByIdCiclista(card.getCiclista()) != null) {
	            banco.add(card);
	            return true;
	        }else{
	            for (Cobranca cobranca : banco) {
	                if (cobranca.getId().equalsIgnoreCase(card.getId())) {
	                    banco.set(banco.indexOf(cobranca), card);
	                    return true;
	                }
	            }
	        }
	        return false;
	    }

	    public Cobranca getCobrancaByIdCiclista(String idciclista) {
	    	for(int i=0; i <= banco.size() - 1; i++) {
				if (banco.get(i).getCiclista().equals(idciclista)) {
					return banco.get(i);
				}
			}
			return null;
		}

		public Boolean deleteData(String id) {
	        for (Cobranca cobranca : banco) {
	            if (cobranca.getId() != null && cobranca.getId().equals(id)) {
	                banco.remove(cobranca);                
	                return true;
	            }
	        }
	        return false;
	    }
	    
		public List<Cobranca> getAllCobrancas() {
			return this.banco;
		}
		
		public Cobranca getCobrancaById(String id) {
			for(int i=0; i <= banco.size() - 1; i++) {
				if (banco.get(i).getId().equals(id)) {
					return banco.get(i);
				}
			}
			return null;
		}

		public boolean createNovaCobranca(Cobranca c) {
			 if (c.getId() != null && this.getCobrancaByIdCiclista(c.getCiclista()) != null) {
	            banco.add(c);
	            return true;
			 }
			 return false;
		}
		
		public Cobranca cobrancaTotalByCiclista(Cobranca c) {
			Integer valorTotal = 0;
				for (Cobranca cobranca : banco) {
					if (cobranca.getStatus() == PagamentoStatus.PENDENTE && cobranca.getCiclista().equals(c.getCiclista())) {
						 valorTotal += cobranca.getValor();
					}
				}
				c.setValor(valorTotal);
				return c;
		}
}

package com.unirio.pm.services;

import com.unirio.pm.domain.Cartao;
import com.unirio.pm.domain.Cobranca;
import com.unirio.pm.domain.Mail;

import controllers.CartaoController;
import controllers.CobrancaController;
import controllers.EmailController;
import io.javalin.http.Context;

public class DBOService {

	private static CobrancaController cobrancaController;
	private static EmailController emailController;
	private static CartaoController cartaoController;
	private static void initCobrancaController() {
		cobrancaController = CobrancaController.getCobrancaController();
	}
	private static void initEmailController() {
		emailController = EmailController.getEmailController();
	}
	private static void initCartaoController() { cartaoController = CartaoController.getCartaoController(); }

	private DBOService() {
		initCartaoController();
		initCobrancaController();
		initEmailController();
	}


	public static Cobranca getCobranca(Cobranca cobranca) {
		initCobrancaController();
		return cobrancaController.getCobranca(cobranca.getId());
	}

	public static boolean addCobranca(Cobranca cobranca) {
		initCobrancaController();
		return cobrancaController.addCobranca(cobranca);
	}

	public static Cobranca realizaCobranca(Context ctx) {
		initCobrancaController();
		return cobrancaController.realizaCobranca(ctx);
	}

	public static boolean validaEmail(String email) {
		initEmailController();
		return emailController.validaEmail(email);
	}

	public static boolean validaCartaoCredito(Cartao cartao) {
		initCartaoController();
		return cartaoController.validaCartaoCredito(cartao);
	}
	public static Cartao novoCartaoCredito(Context ctx) {
		initCartaoController();
		return cartaoController.novoCartaoCredito(ctx);
	}
	public static Mail criarEmailByCtx(Context ctx) {
		initEmailController();
		return emailController.criarEmailByCtx(ctx);
	}
	public static Cobranca criarCobranca(Context ctx) {
		initCobrancaController();
		return cobrancaController.criarCobranca(ctx);
	}
	public static Cobranca adicionaNovaCobranca(Context ctx) {
		initCobrancaController();
		return cobrancaController.adicionaNovaCobranca(ctx);
	}
}

package com.unirio.pm.domain;

public enum PagamentoStatus {
    PENDENTE(1),
    PAGA(2),
    FALHA(3),
    CANCELADA(4),
    OCUPADA(5);

    private Integer status;
    PagamentoStatus(Integer status) {
        this.status = status;
    }
}

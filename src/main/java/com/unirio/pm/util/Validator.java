package com.unirio.pm.util;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.format.ResolverStyle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.unirio.pm.domain.Cartao;

public final class Validator {
	
	public static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
	  
	  
	private Validator() {
		throw new IllegalStateException("ErrorResponse");
	}

	public static boolean isNullOrEmpty(String value) {
		return (value == null || value.equalsIgnoreCase("null") || value.equals(""));
	}
	
	public static boolean validaCartao(Cartao cartao) {
		boolean valida = true;
		 
		if (!cartao.getNumero().matches("^[0-9]{16}$") || !isDataValida(cartao.getValidade()) || !cartao.getCvv().matches("^[0-9]{3,4}$")) {
			return false;
		} 
		return valida;
	}
	
	@SuppressWarnings("unused")
	public static boolean isDataValida(String strDate) {
	    String dateFormat = "uuuu-MM-dd";

	    DateTimeFormatter dateTimeFormatter = DateTimeFormatter
	    .ofPattern(dateFormat)
	    .withResolverStyle(ResolverStyle.STRICT);
	    try {
	        LocalDate date = LocalDate.parse(strDate, dateTimeFormatter);
	        return true;
	    } catch (DateTimeParseException e) {
	       return false;
	    } 
	}
	
	public static boolean notificaEmail(String email) {
        return validaPatternEmail(email);
    }
	

    private static boolean validaPatternEmail(String email) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(email);
        return matcher.find();
    }

	public static boolean validaEmail(String email) {
		return notificaEmail(email);
	}


}

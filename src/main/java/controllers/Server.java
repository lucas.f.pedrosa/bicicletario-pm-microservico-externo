package controllers;

import com.unirio.pm.domain.Cartao;
import com.unirio.pm.domain.Cobranca;
import com.unirio.pm.domain.Mail;
import com.unirio.pm.services.DBOService;
import com.unirio.pm.util.Error;

import io.javalin.Javalin;

public class Server {
	private final static String PATH = "/cobranca";
	private final static String PATH_CARTAO = "/cartaoDeCredito";
	private final static String NOT_FOUND_MESSAGE = "Não encontrado";
	private final static String INVALID_DATA_MESSAGE = "Dados Inválidos";
	private final static String ID_CICLISTA = "/:idCiclista";
	private final static Error ERROR_404 = Error.builder().codigo("404").mensagem(NOT_FOUND_MESSAGE).build();
	private final static Error ERROR_422 = Error.builder().codigo("422").mensagem(INVALID_DATA_MESSAGE).build();

	private static int getHerokuAssignedPort() {
		String herokuPort = System.getenv("PORT");
		if (herokuPort != null) {
			return Integer.parseInt(herokuPort);
		}
		return 7000;
	}
	
	private  Javalin app = Javalin.create()
      
		.get(PATH + "/:idCobranca", ctx -> {
        	String id = ctx.pathParam("idCobranca");
			Cobranca cobranca = DBOService.getCobranca(Cobranca.builder().id(id).build());
			if (cobranca != null) {
				ctx.status(200).json(cobranca);
			} else {
				ctx.status(404).json(ERROR_404);
			}
        })

		.post(PATH, ctx -> {
			Cobranca cobranca = DBOService.criarCobranca(ctx);
			if (cobranca != null) {
				ctx.status(200).json(cobranca);
			} else {
				ctx.status(422).json(ERROR_422);
			}
		})
		
		.post(PATH + ID_CICLISTA, ctx -> {
			Cobranca cobranca = DBOService.realizaCobranca(ctx);
			if (cobranca != null) {
				ctx.status(200).json(cobranca);
			} else {
				ctx.status(422).json(ERROR_422);
			}
		})

		.post("/enviarEmail", ctx -> {
			Mail email = DBOService.criarEmailByCtx(ctx);
			if (!DBOService.validaEmail(email.getEmail())) {
				ctx.status(422).json(ERROR_422);
				return;
			}
			EmailController.enviarEmail(email);
			ctx.json(email).status();
			
		})

		.post("/filaCobranca", ctx -> {
			Cobranca cobranca = DBOService.adicionaNovaCobranca(ctx);
			DBOService.addCobranca(cobranca);
			ctx.json(cobranca).status();
		})

		.post("/validaCartaoDeCredito" + ID_CICLISTA, ctx -> {
			Cartao cartao = DBOService.novoCartaoCredito(ctx);
			if (cartao !=null) {
				ctx.status(200).json(cartao);
			} else {
				ctx.status(422).json(ERROR_422);
			}
		})
		
		.get(PATH_CARTAO + ID_CICLISTA, ctx -> {
			 CartaoController.getCartaoCreditoByIdCiclista(ctx);
		})
		
		.put(PATH_CARTAO + ID_CICLISTA, ctx -> {
			 CartaoController.putCartaoCreditoByIdCiclista(ctx);
		});
		
	public static void main (String[] args) {
		Server app = new Server();
		app.start(getHerokuAssignedPort());
	}
	
   public void start(int port) {
        this.app.start(port);
    }

    public void stop() {
        this.app.stop();
    }
	
	
}

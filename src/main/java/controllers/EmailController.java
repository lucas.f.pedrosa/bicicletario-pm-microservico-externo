package controllers;

import com.fasterxml.uuid.Generators;
import com.unirio.pm.domain.Mail;
import com.unirio.pm.services.JDBCMockEmail;
import com.unirio.pm.util.Validator;

import io.javalin.http.Context;

public class EmailController {
	
	public static final JDBCMockEmail mock = new JDBCMockEmail();
    private static EmailController emailController;

    public static EmailController getEmailController() {
        if (emailController == null) {
            emailController = new EmailController();
        }
        return emailController;
    }
    
    public boolean validaEmail(String email) {
        return Validator.validaEmail(email);
    }

	public static boolean enviarEmail(Mail emailParam) {
		 //envia o email de fato com um servidor de envio de email
		//mas neste caso, como é mockado, apenas verifica se o email existe no banco de dados anntes de enviar
		mock.updateData(emailParam);
		return mock.existeEmail(emailParam.getEmail());
	}

	public Mail criarEmailByCtx(Context ctx) {
		if(!Validator.isNullOrEmpty(ctx.queryParam("email")) && !Validator.isNullOrEmpty(ctx.queryParam("mensagem"))) {
			return Mail.builder().uuid(Generators.timeBasedGenerator().generate().toString()).email(ctx.queryParam("email")).mensagem(ctx.queryParam("mensagem")).build();
		}
		return null;
	}
}
